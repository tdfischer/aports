# Contributor: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
# Maintainer: Iztok Fister, Jr. <iztok@iztok-jr-fister.eu>
pkgname=py3-patsy
pkgver=0.5.5
pkgrel=0
pkgdesc="Describing statistical models in Python using symbolic formulas"
url="https://github.com/pydata/patsy"
arch="noarch"
license="BSD-2-Clause"
depends="
	python3
	py3-numpy
	py3-scipy
	py3-six
	"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-pytest-xdist py3-pytest-cov"
subpackages="$pkgname-pyc"
source="https://github.com/pydata/patsy/archive/v$pkgver/patsy-$pkgver.tar.gz"
builddir="$srcdir/patsy-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
22ed936447c703ee9c496291f08f6fa7c5d5828ae2f5c56909f48f9484c0e71d4fef879e13345b7634c9a8a53b8396d754a0fe407e2b667b943a88753a74cfb6  patsy-0.5.5.tar.gz
"
